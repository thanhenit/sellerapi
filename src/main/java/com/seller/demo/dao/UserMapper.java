package com.seller.demo.dao;

import com.seller.demo.model.Users;
import com.seller.demo.model.Vendor;
import com.seller.demo.model.VendorRequest;


import java.util.List;
import java.util.Map;

public interface UserMapper {

    List<Users> findAll();

    void insertUser(Users users);

    List<Object> getAll();

    Users userLogin(Users users);

    Vendor selectProfile(int vendor_id);

    Users findUserName(String username);

    void insertVendor(Vendor vendorform);

    int findLastID();

    String findLastIDUser();

//    void updatePassword(VendorRequest vendor);
//
//    void updatePasswordLogin(VendorRequest vendor);


    void updatePassword(Map vendormap);

    void updatePasswordLogin(Map vendormap);

    String checkEmail(String mail);
}
