package com.seller.demo;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;


@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {





    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors()
                    .and()
                .csrf()
                    .disable()
                .authorizeRequests()
                    .antMatchers("/seller/login").permitAll() // Cho phép tất cả mọi người truy cập vào 2 địa chỉ này
                    .antMatchers("/seller/signup").permitAll()
                    .antMatchers("/seller/forgot").permitAll()
                    .antMatchers("/seller/forgot/{otp}").permitAll()
                    .antMatchers("/seller/forgot/{email}/{psw}").permitAll()
                    .antMatchers("/seller/header").permitAll()
                    .anyRequest().authenticated(); // Tất cả các request khác đều cần phải xác thực mới được truy cập

    }
}