package com.seller.demo.jwt;

import com.seller.demo.model.Users;
import com.seller.demo.model.Vendor;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Map;

@Component
@Slf4j
public class JSONTOKEN {
    private final String JWT_SECRET = "thanh";
    private final long JWT_EXPIRATION = 604800000L;
    public String generateToken(Vendor vendor) {
        Date now = new Date();
        Date expiryDate = new Date(now.getTime() + JWT_EXPIRATION);
        String payload = "data :";
        return Jwts.builder()
                .setSubject("Vendor")
                .setIssuedAt(now)
                .setExpiration(expiryDate)
                .claim(payload,vendor)
                .signWith(SignatureAlgorithm.HS256, JWT_SECRET)
                .compact();
    }


    public Long getUserIdFromJWT(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(JWT_SECRET)
                .parseClaimsJws(token)
                .getBody();

        return Long.parseLong(claims.getSubject());
    }

    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(JWT_SECRET).parseClaimsJws(authToken);
            return true;
        } catch (MalformedJwtException ex) {
            log.error("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            log.error("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            log.error("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            log.error("JWT claims string is empty.");
        }
        return false;
    }
}
