package com.seller.demo.model;


import lombok.Data;

import java.util.Map;

@Data
public class LoginResponse {
    private String accessToken ;
    private String tokenType = "Bearer ";
    private String Status;
    private String Messenger;

    public LoginResponse(String Status , String Message,String accessToken){
        this.Status=Status;
        this.Messenger=Message;
        this.accessToken=accessToken;

    }
    public LoginResponse(String Status , String Message){
        this.Status=Status; this.Messenger=Message;
    }
}
