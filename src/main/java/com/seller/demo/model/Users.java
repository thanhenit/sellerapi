package com.seller.demo.model;



public class Users {

    private String SYSOP_NO;
    private String LOGIN_ID;
    private String PASSWD_TXT;
    private String LOGIN_ST_CD;
    private int VENDOR_ID;


    public Users(String SYSOP_NO,String LOGIN_ID,String PASSWD_TXT,String LOGIN_ST_CD,int VENDOR_ID){
        this.SYSOP_NO=SYSOP_NO;
        this.LOGIN_ID=LOGIN_ID;
        this.PASSWD_TXT=PASSWD_TXT;
        this.LOGIN_ST_CD=LOGIN_ST_CD;
        this.VENDOR_ID=VENDOR_ID;
    }


    public int getVENDOR_ID() {
        return VENDOR_ID;
    }

    public String getLOGIN_ST_CD() {
        return LOGIN_ST_CD;
    }

    public String getLOGIN_ID() {
        return LOGIN_ID;
    }

    public String getPASSWD_TXT() {
        return PASSWD_TXT;
    }

    public String getSYSOP_NO() {
        return SYSOP_NO;
    }

    public void setLOGIN_ID(String LOGIN_ID) {
        this.LOGIN_ID = LOGIN_ID;
    }

    public void setPASSWD_TXT(String PASSWD_TXT) {
        this.PASSWD_TXT = PASSWD_TXT;
    }

    public void setSYSOP_NO(String SYSOP_NO) {
        this.SYSOP_NO = SYSOP_NO;
    }

    public void setLOGIN_ST_CD(String LOGIN_ST_CD) {
        this.LOGIN_ST_CD = LOGIN_ST_CD;
    }

    public void setVENDOR_ID(int VENDOR_ID) {
        this.VENDOR_ID = VENDOR_ID;
    }
}
