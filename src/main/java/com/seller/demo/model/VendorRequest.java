package com.seller.demo.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VendorRequest {

    private  String EMAIL;
    private String PASSWORD;
    private String FIRST_NAME;
    private String LAST_NAME;



    public  VendorRequest(String EMAIL,String PASSWORD,String FIRST_NAME,String LAST_NAME){
        this.EMAIL=EMAIL;
        this.PASSWORD=PASSWORD;
        this.FIRST_NAME=FIRST_NAME;
        this.LAST_NAME=LAST_NAME;
    }
//    public  VendorRequest(String EMAIL,String PASSWORD){
//        this.EMAIL=EMAIL;
//        this.PASSWORD=PASSWORD;
//
//    }
}

