package com.seller.demo.model;


public class Vendor {
    private int VENDOR_ID;
    private String VENDOR_NAME;
    private String STATUS;
    private String EMAIL;
    private String TELEPHONE;
    private String FAX;
    private String CONTRACT_NUMBER;
    private String PASSWORD;
    private String TAX_CODE;


    public Vendor(int VENDOR_ID,String VENDOR_NAME, String STATUS,String EMAIL, String PASSWORD) {
        this.VENDOR_ID=VENDOR_ID;
        this.VENDOR_NAME = VENDOR_NAME;
        this.STATUS=STATUS;
        this.EMAIL = EMAIL;
        this.PASSWORD = PASSWORD;
    }



    public int getVENDOR_ID() {
        return VENDOR_ID;
    }


    public String getCONTRACT_NUMBER() {
        return CONTRACT_NUMBER;
    }


    public String getEMAIL() {
        return EMAIL;
    }


    public String getTAX_CODE() {
        return TAX_CODE;
    }


    public String getVENDOR_NAME() {
        return VENDOR_NAME;
    }

    public String getPASSWORD() {
        return PASSWORD;
    }

    public void setVENDOR_ID(int VENDOR_ID) {
        this.VENDOR_ID = VENDOR_ID;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }


    public void setPASSWORD(String PASSWORD) {
        this.PASSWORD = PASSWORD;
    }

    @Override
    public String toString() {
        return String.format("VENDOR_NAME :" + VENDOR_NAME + " STAT US:" + STATUS + " EMAIL :" + EMAIL);
    }
}