package com.seller.demo.resources;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.hash.Hashing;
import com.seller.demo.dao.UserMapper;
import com.seller.demo.model.LoginResponse;
import com.seller.demo.model.Users;
import com.seller.demo.model.Vendor;
import com.seller.demo.jwt.JSONTOKEN;
import com.seller.demo.model.VendorRequest;
import org.apache.ibatis.binding.BindingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

@RestController
@RequestMapping("/seller")
public class UserResource {

    @Autowired
    private JSONTOKEN jsontoken;

    @Autowired
    private UserMapper userMapper;

    static char otpcheck[];
    static String emailforgot;

    static int checkforgot;


    public UserResource(UserMapper userMapper) {
        this.userMapper = userMapper;
    }


    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Users> getAll() {
        System.out.println("here !!!");
        List<Users> a = userMapper.findAll();

        return a;
    }

    @RequestMapping(value = "/getall", method = RequestMethod.GET)
    public List<Object> all() {
        List<Object> all = userMapper.getAll();
        return all;
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST,
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ResponseBody
    public LoginResponse login(@RequestBody Users userform) {
        Map<String, String> responseMap = new HashMap<String, String>();
        String Status = null;
        String Message=null;
        try {
            Users user = userMapper.userLogin(userform);

            ObjectMapper mapper = new ObjectMapper();
            String json = null;
            String password = userform.getPASSWD_TXT();
            String[] hashedPasswordMap = (user.getPASSWD_TXT() + "::").split(":", 3);
            String hashedPassword = hashedPasswordMap[0];
            String salt = hashedPasswordMap[1];
            Vendor vendor = userMapper.selectProfile(user.getVENDOR_ID());
            if (!hashedPassword.equals(
                    Hashing.sha256().hashString(salt + password, StandardCharsets.UTF_8).toString())) {
                 Status = "10";
                 Message = "Password wrong";;
                return new LoginResponse(Status,Message);
            } else {
                if (user.getLOGIN_ST_CD().equals("LOG001")) {
                     Status = "0";
                     Message = "OK";;
                    String accessToken = jsontoken.generateToken(vendor);
                    return new LoginResponse(Status,Message, accessToken);
                } else if (!user.getLOGIN_ST_CD().equals("LOG001")) {
                     Status = "11";
                     Message = "Account not active";;
                    return new LoginResponse(Status,Message);
                }
            }
            return new LoginResponse(Status,Message);
        } catch (Exception e) {
            Status = "1";
            Message = "Check the data type";
            responseMap.put("Status", "1");
            responseMap.put("Message", "Check the data type");
            System.out.println(e.getMessage());
            return new LoginResponse(Status,Message);
        }


    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST,
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ResponseBody
    public Map<String, String> signup(@RequestBody VendorRequest vendorform) {
        Map<String, String> responseMap = new HashMap<String, String>();
        int vendorLastId = userMapper.findLastID() + 1;
        String userLastId = userMapper.findLastIDUser();
        String[] str = userLastId.split("S");
        int lastID = Integer.parseInt(str[1]) + 1;
        String SYSNO = "S" + lastID;
        String vendor_name = vendorform.getFIRST_NAME() + " " + vendorform.getLAST_NAME();
        String salt = "sa";
        String late = "1";
        String pwd = Hashing.sha256().hashString(salt + vendorform.getPASSWORD(), StandardCharsets.UTF_8).toString() + ":" + salt + ":" + late;
        System.out.println(pwd);
        String status = "A";
        try {
            Vendor vendorInstert = new Vendor(vendorLastId, vendor_name, status, vendorform.getEMAIL(), pwd);
            userMapper.insertVendor(vendorInstert);
            responseMap.put("Status", "0");
            responseMap.put("Message", "OK");
            try {
                Users users = new Users(SYSNO, vendorform.getEMAIL(), pwd, "LOG001", vendorLastId);
                System.out.println(SYSNO + vendorform.getEMAIL() + pwd + "LOG001" + vendorLastId);
                userMapper.insertUser(users);

            } catch (BindingException e) {
                System.out.println(e.getMessage());
                responseMap.put("Message", "Create Vendor Login fail");
            }

        } catch (DuplicateKeyException e) {
            responseMap.put("Status", "500");
            responseMap.put("Message", "duplicate email ");
        }

        return responseMap;
    }

    @RequestMapping(value = "/forgot", method = RequestMethod.POST,
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ResponseBody
    public Map<String, Object> forgotpsw(@RequestBody Map<String,String> vendor) {
        Map<String, Object> responseMap = new HashMap<String, Object>();
        try{
            String mail=vendor.get("EMAIL");
            String check=userMapper.checkEmail(mail);
            if(check==null && mail!=null){
                responseMap.put("Status", "10");
                responseMap.put("Message", "mail does not exist ");
                responseMap.put("EMAIL", vendor.get("EMAIL"));
            }
            else if(mail==null){
                responseMap.put("Status", "1");
                responseMap.put("Message", "Error check param ");
            }
            else {
                try {
                    String numbers = "0123456789";
                    Random rndm_method = new Random();
                    char[] otp = new char[4];
                    for (int i = 0; i < 4; i++) {

                        otp[i] = numbers.charAt(rndm_method.nextInt(numbers.length()));
                    }
                    responseMap.put("Status", "0");
                    responseMap.put("Message", "OK");
                    responseMap.put("EMAIL", vendor.get("EMAIL"));
                    responseMap.put("OTP", otp);
                    otpcheck = otp;
                    emailforgot = vendor.get("EMAIL");
                    return responseMap;
                } catch (Exception e) {
                    responseMap.put("Status", "500");
                    responseMap.put("MESSAGE", "Fail");
                    responseMap.put("EMAIL", vendor.get("EMAIL"));
                    return responseMap;
                }
            }
        }catch (Exception e){
            System.out.println(e.getMessage());

        }
        return responseMap;
    }

    @RequestMapping(value = "/forgot/{otp}", method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ResponseBody
    public Map<String, Object> checkotp(@PathVariable("otp") String otp) {
        Map<String, Object> responseMap = new HashMap<String, Object>();
        try {
            if (otp.equals(String.valueOf(otpcheck))) {
                responseMap.put("Status", "0");
                responseMap.put("Message", "OK");
                responseMap.put("EMAIL", emailforgot);
                checkforgot=1;
            } else {
                responseMap.put("Status", "10");
                responseMap.put("Message", "WRONG");
                checkforgot=0;
            }
            return responseMap;
        } catch (Exception e) {
            responseMap.put("Status", "500");
            responseMap.put("Message", "Fail");
            checkforgot=0;
            return responseMap;
        }
    }


    @RequestMapping(value = "/forgot/{email}", method = RequestMethod.PUT,
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ResponseBody
    public Map<String, Object> newpsw(@PathVariable("email") String email,HttpServletRequest request) {
        Map<String, Object> responseMap = new HashMap<String, Object>();
        if(checkforgot==1){
            try {
                String password= request.getParameter("password");
                String salt = "sa";
                String late = "1";
                String pwd = Hashing.sha256().hashString(salt + password, StandardCharsets.UTF_8).toString() + ":" + salt + ":" + late;
                Map<String,String> vendorMap = new HashMap<>();
                vendorMap.put("EMAIL",email);
                vendorMap.put("PASSWORD",pwd);
//            VendorRequest vendor = new VendorRequest(vendorRequest.getEMAIL(),pwd);
                userMapper.updatePassword(vendorMap);
                userMapper.updatePasswordLogin(vendorMap);
                responseMap.put("Status", "0");
                responseMap.put("Message", "OK");
                responseMap.put("EMAIL",email);
                checkforgot=0;
                return responseMap;
            } catch (HttpMessageConversionException e) {
                responseMap.put("Status", "500");
                responseMap.put("Message", "Fail");
                checkforgot=0;
                return responseMap;
            }
        }
        else {
            try{
                responseMap.put("Status", "405");
                responseMap.put("Message", "Unsupported operation");
            }catch (Exception e){
                System.out.println(e.getMessage());
                responseMap.put("Status", "500");
                responseMap.put("Message", "Fail");
                checkforgot=0;
                return responseMap;
            }
        }
        return responseMap;
    }

    @RequestMapping(value = "/header", method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ResponseBody
    public String testheader(HttpServletRequest request) {
        Map<String, String> responseMap = new HashMap<>();
       String header= request.getHeader("Authorization");
            System.out.println(header.substring(7));
        try {
            System.out.println(header);
            return null;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }



}
